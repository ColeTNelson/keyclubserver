package k12.oostburg.cnelson.statics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;

import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.Member;

public class Configuration {
	public static void save(Event e){
		YamlConfiguration config = new YamlConfiguration();
		File file = new File(Database.getDirEvents()+"\\"+e.getUUID().toString()+".yml");
		config.set("name", e.getName());
		config.set("address", e.getAddress());
		config.set("date", e.getDate());
		config.set("time", e.getTime());
		config.set("hours", e.getHours());
		config.set("info", e.getInfo());
		config.set("max-volunteers", e.getMaxVolunteers());
		ArrayList<String> strVolunteers = new ArrayList<String>();
		for(Member m : e.getVolunteers()){
			strVolunteers.add(m.getUUID().toString());
		}
		config.set("volunteers", strVolunteers);
		config.set("archived", e.isArchived());
		try {
			config.save(file);
		} catch (IOException e2) {
			Database.getLogger().log(Level.SEVERE, "Unable to save ANY aspects of Event "+e.getUUID()+" ["+e.getName()+"]", e2);
		}
	}
	
	public static void save(Member m){
		YamlConfiguration config = new YamlConfiguration();
		File file = new File(Database.getDirMembers()+"\\"+m.getUUID().toString()+".yml");
		config.set("member-info.first-name", m.getFirstName());
		config.set("member-info.last-name", m.getLastName());
		config.set("member-info.email", m.getEmail());
		config.set("member-info.password", Encrypter.encrypt(m.getPassword()));
		config.set("member-info.grade", m.getGrade().toString());
		config.set("member-info.role", m.getRole().toString());
		try {
			config.save(file);
		} catch (IOException e) {
			Database.getLogger().log(Level.SEVERE, "Unable to save ANY aspects of Member "+m.getUUID()+" ["+m.getFullName()+"]", e);
		}
	}

	public static void delete(Event e) {
		new File(Database.getDirEvents()+"\\"+e.getUUID().toString()+".yml").delete();
	}
}
