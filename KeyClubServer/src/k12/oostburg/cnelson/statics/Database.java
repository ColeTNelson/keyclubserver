package k12.oostburg.cnelson.statics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import k12.oostburg.cnelson.enums.Grade;
import k12.oostburg.cnelson.enums.Role;
import k12.oostburg.cnelson.objects.Email;
import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.Member;

public class Database {
	
	private static File dir = new File("C:\\Users\\Cole Nelson\\Desktop\\Key Club\\");
	private static File fileOptions = new File("C:\\Users\\Cole Nelson\\Desktop\\Key Club\\options.yml");
	private static File dirEvents = new File(dir.getAbsolutePath()+"\\Events\\");
	private static File dirMembers = new File(dir.getAbsolutePath()+"\\Members\\");
	private static Logger logger = Logger.getLogger("Key Club");
	private static ArrayList<Member> members = new ArrayList<Member>();
	private static ArrayList<Event> events = new ArrayList<Event>();
	private static String password;
	private static ArrayList<Event> archivedEvents = new ArrayList<Event>();
	
	public static void init() throws Exception{
		initDirectories();
		initOptions();
		initMembers();
		initEvents();
		for(Member m : Database.getMembers()){
			System.out.println(m.getEmail() + " : " + m.getPassword());
		}
	}
	
	private static void initOptions() throws FileNotFoundException, IOException, InvalidConfigurationException{
		YamlConfiguration config = new YamlConfiguration();
		if(!fileOptions.exists()){
			fileOptions.createNewFile();
		}
		config.load(fileOptions);
		Database.password = Encrypter.decrypt(config.getString("email-password"));
	}
	
	private static void initDirectories(){
		if(!dir.exists()){
			dir.mkdir();
		}
		if(!dirMembers.exists()){
			dirMembers.mkdir();
		}
		if(!dirEvents.exists()){
			dirEvents.mkdir();
		}
	}
	
	public static Logger getLogger(){
		return logger;
	}
	
	public static void initMembers() throws Exception{
		for(File f : dirMembers.listFiles()){
			YamlConfiguration config = new YamlConfiguration();
			config.load(f);
			UUID uuid = UUID.fromString(stripYml(f.getName()));
			String fName = config.getString("member-info.first-name");
			String lName = config.getString("member-info.last-name");
			String password = Encrypter.decrypt(config.getString("member-info.password"));
			String email = config.getString("member-info.email");
			String grade = config.getString("member-info.grade");
			String role = config.getString("member-info.role");
			Database.addMember(new Member(uuid, fName, lName, email, Grade.valueOf(grade), Role.valueOf(role), password));
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void initEvents() throws Exception{
		for(File f : dirEvents.listFiles()){
			YamlConfiguration config = new YamlConfiguration();
			config.load(f);
			UUID uuid = UUID.fromString(stripYml(f.getName()));
			String name = config.getString("name");
			String address = config.getString("address");
			String date = config.getString("date");
			String time = config.getString("time");
			boolean archived = config.getBoolean("archived");
			int hours = config.getInt("hours");
			int max = config.getInt("max-volunteers");
			ArrayList<String> info = (ArrayList<String>) config.getList("info");
			ArrayList<Member> volunteers = new ArrayList<Member>();
			for(String s : (ArrayList<String>) config.getList("volunteers")){
				volunteers.add(MemberIdentifier.getMember(UUID.fromString(s)));
			}
			if(archived){
				Database.addArchivedEvent(new Event(uuid, volunteers, info, name, address, date, time, hours, max, archived));
			}else{
				Database.addEvent(new Event(uuid, volunteers, info, name, address, date, time, hours, max, archived));
			}
		}
	}
	
	public static void addEvent(Event e){
		events.add(e);
	}
	
	public static void addArchivedEvent(Event e){
		archivedEvents.add(e);
	}
	
	public static boolean removeEvent(UUID uuid){
		Event target = null;
		for(Event e : Database.getEvents()){
			if(e.getUUID().equals(uuid)){
				target = e;
			}
		}
		if(target==null){
			return false;
		}
		events.remove(target);
		return true;
	}
	
	public static boolean removeArchivedEvent(UUID uuid){
		Event target = null;
		for(Event e : Database.getArchivedEvents()){
			if(e.getUUID().equals(uuid)){
				target = e;
			}
		}
		if(target==null){
			return false;
		}
		archivedEvents.remove(target);
		return true;
	}
	
	public static void addMember(Member m){
		members.add(m);
	}
	
	public static void removeMember(Member m){
		members.remove(m);
	}
	
	private static String stripYml(String s){
		return s.substring(0, s.length()-4);
	}
	
	public static UUID generateMemberUUID(){
		if(dirMembers.list()==null){
			return UUID.randomUUID();
		}
		UUID uuid = UUID.randomUUID();
		while(Arrays.asList(dirMembers.list()).contains(uuid.toString())){
			uuid = UUID.randomUUID();
		}
		return uuid;
	}
	
	public static UUID generateEventUUID(){
		if(dirEvents.list()==null){
			return UUID.randomUUID();
		}
		UUID uuid = UUID.randomUUID();
		while(Arrays.asList(dirEvents.list()).contains(uuid.toString())){
			uuid = UUID.randomUUID();
		}
		return uuid;
	}
	
	public static ArrayList<Member> getMembers() {
		return members;
	}
	
	public static ArrayList<Event> getEvents(){
		return events;
	}
	
	public static ArrayList<Event> getArchivedEvents(){
		return archivedEvents;
	}
	
	public static File getDirMembers(){
		return dirMembers;
	}
	
	public static File getDirEvents(){
		return dirEvents;
	}

	public static Event getEvent(UUID obj) {
		for(Event e : events){
			if(e.getUUID().equals(obj)){
				return e;
			}
		}
		for(Event e : archivedEvents){
			if(e.getUUID().equals(obj)){
				return e;
			}
		}
		return null;
	}

	public static String getPassword(){
		return Database.password;
	}
	
	public static boolean validatePassword() {
		return EmailSender.send(new Email("oostburgkeyclubtest@gmail.com", Database.password, "I solemnly swear that I am up to no good.", new ArrayList<String>()));
	}
}
