package k12.oostburg.cnelson.statics;

import k12.oostburg.cnelson.server.SocketListener;

public class KeyClubServer {

	public static void main(String[] args) throws Exception{
		Database.init();
		new Thread(new SocketListener()).start();
		System.out.println("Done!");
	}
}
