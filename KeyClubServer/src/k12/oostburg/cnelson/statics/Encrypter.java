package k12.oostburg.cnelson.statics;
import java.math.BigInteger;
import java.security.*;
import java.util.Random;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.*;

public class Encrypter {
    
	private static final String ALGO = "AES";
	private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y' };

    public static String encrypt(String Data){
    	String encryptedValue;
    	try{
    		 Key key = generateKey();
    	        Cipher c = Cipher.getInstance(ALGO);
    	        c.init(Cipher.ENCRYPT_MODE, key);
    	        byte[] encVal = c.doFinal(Data.getBytes());
    	        encryptedValue = new BASE64Encoder().encode(encVal);
    	}catch(Exception e){
    		e.printStackTrace();
    		return "thereisnourflevel";
    	}
        return encryptedValue;
    }

    public static String decrypt(String encryptedData){
    	String decryptedValue;
    	try{
    		Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
            byte[] decValue = c.doFinal(decordedValue);
            decryptedValue = new String(decValue);
    	}catch(Exception e){
    		e.printStackTrace();
    		return "thereisnourflevel";
    	}
        return decryptedValue;
    }
    private static Key generateKey(){
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }
    public static String generatePassword() throws Exception{
    	 SecureRandom random = new SecureRandom();
    	 String password = new BigInteger(130, random).toString(32);
    	 return Encrypter.encrypt(password.substring(password.length()-6));
    }
}
