package k12.oostburg.cnelson.statics;

import java.util.ArrayList;
import java.util.UUID;

import k12.oostburg.cnelson.enums.ResponseCode;
import k12.oostburg.cnelson.objects.LoginPair;
import k12.oostburg.cnelson.objects.Member;

public class MemberIdentifier {
	public static Member getMember(String s){
		ArrayList<Member> matches = getMembers(s);
		if(matches.isEmpty() || matches.size() > 1){
			return null;
		}
		return matches.get(0);
	}
	public static Member getMemberByEmail(String s){
		for(Member m : Database.getMembers()){
			if(m.getEmail().equalsIgnoreCase(s)){
				return m;
			}
		}
		return null;
	}
	public static Member getMember(UUID uuid){
		String suuid = uuid.toString();
		for(Member m : Database.getMembers()){
			if(suuid.equalsIgnoreCase(m.getUUID().toString())){
				return m;
			}
		}
		return null;
	}
	public static Member getMember(LoginPair lp) throws Exception{
		String password = Encrypter.decrypt(lp.getPassword());
		for(Member m : Database.getMembers()){
			if(m.getEmail().equalsIgnoreCase(lp.getEmail()) && m.getPassword().equals(password)){
				return m;
			}
		}
		return null;
	}
	public static ArrayList<Member> getMembers(String s){
		s = s.toLowerCase();
		ArrayList<Member> matches = new ArrayList<Member>();
		if(s.contains(" ")){
			String[] split = s.split(" ");
			for(Member m : Database.getMembers()){
				if(m.getFirstName().toLowerCase().startsWith(split[0]) && m.getLastName().toLowerCase().startsWith(split[1])){
					matches.add(m);
				}
			}
			if(matches.isEmpty()){
				for(Member m : Database.getMembers()){
					if(m.getFirstName().toLowerCase().startsWith(split[1])&& m.getLastName().toLowerCase().startsWith(split[0])){
						matches.add(m);
					}
				}
			}
		}else{
			for(Member m : Database.getMembers()){
				if(m.getFirstName().toLowerCase().startsWith(s)){
					matches.add(m);
				}
			}
			if(matches.isEmpty()){
				for(Member m : Database.getMembers()){
					if(m.getLastName().toLowerCase().startsWith(s)){
						matches.add(m);
					}
				}	
			}
		}
		return matches;
	}
}
