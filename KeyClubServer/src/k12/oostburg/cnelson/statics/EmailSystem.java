package k12.oostburg.cnelson.statics;

import java.util.ArrayList;

import k12.oostburg.cnelson.objects.Email;
import k12.oostburg.cnelson.objects.Member;

public class EmailSystem implements Runnable{
	
	ArrayList<Email> emails;
	
	public EmailSystem(ArrayList<Email> emails){
		this.emails = emails;
	}
	
	@Override
	public void run(){
		for(Email e : emails){
			e.setPassword(Database.getPassword());
			Member m = MemberIdentifier.getMember(e.getMember().getUUID());
			if(m!=null){
				ArrayList<String> newBody = new ArrayList<String>();
				for(String line : e.getBody()){
					line = line.replaceAll("\\[FIRST_NAME\\]", m.getFirstName());
					line = line.replaceAll("\\[LAST_NAME\\]", m.getLastName());
					line = line.replaceAll("\\[FULL_NAME\\]", m.getFullName());
					line = line.replaceAll("\\[PASSWORD\\]", m.getPassword());
					line = line.replaceAll("\\[EMAIL\\]", m.getEmail());
					newBody.add(line);
				}
				e.setBody(newBody);
			}
			EmailSender.send(e);
		}
	}
}
