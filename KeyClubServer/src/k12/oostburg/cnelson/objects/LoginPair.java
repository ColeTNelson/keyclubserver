package k12.oostburg.cnelson.objects;

import java.io.Serializable;

public class LoginPair implements Serializable{
	
	private static final long serialVersionUID = -4777284642390574959L;
	private String email;
	private String password;
	
	public LoginPair(String email, String password){
		this.email = email;
		this.password = password;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public String getPassword(){
		return this.password;
	}
}
