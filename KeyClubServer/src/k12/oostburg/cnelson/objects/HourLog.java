package k12.oostburg.cnelson.objects;

public class HourLog {
	private Member verifee = null;
	private Event e;
	
	public HourLog(Event e){
		this.e = e;
	}
	
	public HourLog(Event e, Member verifee){
		this.e = e;
		this.verifee = verifee;
	}

	public Member getVerifee() {
		return verifee;
	}

	public void setVerifee(Member verifee) {
		this.verifee = verifee;
	}

	public Event getE() {
		return e;
	}

	public void setE(Event e) {
		this.e = e;
	}
	
	public boolean isValid(){
		if(verifee==null){
			return false;
		}
		return true;
	}
}
