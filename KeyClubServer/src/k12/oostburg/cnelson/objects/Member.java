package k12.oostburg.cnelson.objects;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;

import k12.oostburg.cnelson.enums.Grade;
import k12.oostburg.cnelson.enums.Role;
import k12.oostburg.cnelson.statics.Configuration;
import k12.oostburg.cnelson.statics.Database;
import k12.oostburg.cnelson.statics.Encrypter;

public class Member implements Serializable {
	
	private static final long serialVersionUID = 4815223163680892363L;
	private ArrayList<HourLog> hoursheet = new ArrayList<HourLog>();
	private UUID uuid;
	private String fName;
	private String lName;
	private String email;
	private String password;
	private Grade grade;
	private Role role;
	
	public Member(UUID uuid, String fName, String lName, String email, Grade grade, Role role, String password){
		this.uuid = uuid;
		this.fName = fName;
		this.lName = lName;
		this.email = email;
		this.grade = grade;
		this.role = role;
		this.password = password;
	}

	public ArrayList<HourLog> getHourSheet() {
		return hoursheet;
	}

	public void setHourSheet(ArrayList<HourLog> hoursheet) {
		this.hoursheet = hoursheet;
	}

	public UUID getUUID() {
		return uuid;
	}

	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}

	public String getFirstName() {
		return fName;
	}

	public void setFirstName(String fName) {
		this.fName = fName;
	}
	
	public String getLastName() {
		return lName;
	}

	public void setLastName(String lName) {
		this.lName = lName;
	}

	public String getFullName(){
		return this.fName + " " + this.lName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String toString(){
		return lName+", "+fName+"\n - Grade: "+grade.toString()+"\n - Role: "+role.toString();
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getPassword(){
		return password;
	}
	
	private void writeObject(ObjectOutputStream o) throws IOException{
		o.defaultWriteObject();  
	}
			  
	private void readObject(ObjectInputStream o) throws IOException, ClassNotFoundException {
		o.defaultReadObject();
	}
	
	public YamlConfiguration generateFile() throws Exception{
		YamlConfiguration config = new YamlConfiguration();
		config.set("member-info.first-name", this.getFirstName());
		config.set("member-info.last-name", this.getLastName());
		config.set("member-info.email", this.getEmail());
		config.set("member-info.password", Encrypter.encrypt(this.getPassword()));
		config.set("member-info.grade", this.getGrade().toString());
		config.set("member-info.role", this.getRole().toString());
		return config;
	}
	
	public File getConfigFile(){
		return new File(Database.getDirMembers().getPath()+"\\"+this.getUUID().toString()+".yml");
	}
	
	public LoginPair getLoginPair(){
		return new LoginPair(this.getEmail(), Encrypter.encrypt(this.getPassword()));
	}
	
	public void save(){
		Configuration.save(this);
	}
	
	public boolean isAdministrative(){
		if(this.getRole().equals(Role.MEMBER)){
			return false;
		}
		return true;
	}
}
