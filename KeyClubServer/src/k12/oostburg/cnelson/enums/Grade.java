package k12.oostburg.cnelson.enums;

public enum Grade {
	FRESHMAN,
	SOPHOMORE,
	JUNIOR,
	SENIOR;
	
	public static Grade valueOfNumber(Integer num){
		return valueOfNumber(num.toString());
	}
	
	public static Grade valueOfNumber(String num){
		if(num.equals("9")){
			return Grade.FRESHMAN;
		}else if(num.equals("10")){
			return Grade.SOPHOMORE;
		}else if(num.equals("11")){
			return Grade.JUNIOR;
		}else if(num.equals("12")){
			return Grade.SENIOR;
		}
		return Grade.FRESHMAN;
	}
}
