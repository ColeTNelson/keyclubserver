package k12.oostburg.cnelson.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.enums.ResponseCode;
import k12.oostburg.cnelson.objects.Email;
import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.HourLog;
import k12.oostburg.cnelson.objects.LoginPair;
import k12.oostburg.cnelson.objects.Member;
import k12.oostburg.cnelson.statics.Configuration;
import k12.oostburg.cnelson.statics.Database;
import k12.oostburg.cnelson.statics.EmailSystem;
import k12.oostburg.cnelson.statics.Encrypter;
import k12.oostburg.cnelson.statics.MemberIdentifier;

public class Handler implements Runnable {
	
    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private boolean LOGGED = false;
    private UUID laPersona;
    
    public Handler(Socket socket) {
        this.socket = socket;
    }
    
    public void run() {
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            out.flush();
            while(!LOGGED){
            	Object inc = in.readObject();
             	if(inc instanceof LoginPair){
             		LoginPair lp = (LoginPair) inc;
             		if(MemberIdentifier.getMember(lp)==null){
             			out.writeObject(ResponseCode.INCORRECT_LOGIN);
             		}else{
             			laPersona = MemberIdentifier.getMember(lp).getUUID();
             			Database.getLogger().log(Level.INFO, MemberIdentifier.getMember(laPersona).getFullName()+" has connected!");
             			out.writeObject(ResponseCode.SUCCESS);
             			LOGGED=true;
             		}
             	}else if(inc instanceof Request){
             		if(inc.equals(Request.EMAIL)){
             			doSendEmails();
             		}else if(inc.equals(Request.RESET_PASSWORD_OTHER)){
             			doResetPassword(false);
             		}else if(inc.equals(Request.GET_MEMBER)){
             			doGetMember();
             		}
             	}else{
             		out.writeObject(ResponseCode.REQ_LOGIN);
             		continue;
             	}
            }
            while (true) {
            	if(in.markSupported()){
            		in.reset();
            	}
            	out.reset();
            	Object inc = in.readObject();
            	if(inc instanceof Request){
            		Request req = (Request) inc;
            		if(req.equals(Request.EVENTS)){
            			out.writeObject(Database.getEvents());
            		}else if(req.equals(Request.SWITCH_ARCHIVE)){
            			doSwitchArchive();
            		}else if(req.equals(Request.ARCHIVED_EVENTS)){
            			out.writeObject(Database.getArchivedEvents());
            		}else if(req.equals(Request.GET_MEMBER)){
            			doGetMember();
            		}else if(req.equals(Request.IDENTIFY)){
            			doIdentify();
            		}else if(req.equals(Request.UPDATE_EVENT)){
            			doUpdateEvent();
            		}else if(req.equals(Request.RESET_PASSWORD)){
            			doResetPassword(true);
            		}else if(req.equals(Request.GET_MEMBERS)){
            			out.writeObject(Database.getMembers());
            		}else if(req.equals(Request.RESET_PASSWORD_OTHER)){
            			doResetPassword(false);
            		}else if(req.equals(Request.UPDATE_EMAIL_SELF)){
            			doUpdateEmail(true);
            		}else if(req.equals(Request.UPDATE_EMAIL)){
            			doUpdateEmail(false);
            		}else if(req.equals(Request.ADD_EVENT)){
            			doAddEvent();
            		}else if(req.equals(Request.REMOVE_EVENT)){
            			doRemoveEvent();
            		}else if(req.equals(Request.ADD_HOURLOG)){
            			doAddHourLog();
            		}else if(req.equals(Request.REMOVE_HOURLOG)){
            			doRemoveHourLog();
            		}else if(req.equals(Request.SET_PASSWORD)){
            			doSetPassword(false);
            		}else if(req.equals(Request.GET_EVENT)){
            			doGetEvent();
            		}else if(req.equals(Request.SET_PASSWORD_SELF)){
            			doSetPassword(true);
            		}else if(req.equals(Request.EMAIL)){
            			doSendEmails();
            		}else if(req.equals(Request.SELF)){
            			out.writeObject(MemberIdentifier.getMember(laPersona));
            		}else{
            			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
            		}	
            	}else{
            		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
            	}
            }
        } catch (IOException e) {}
        catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
				if(LOGGED==true){
					Database.getLogger().log(Level.INFO, "Connection aborted with "+MemberIdentifier.getMember(laPersona).getFullName());
				}else{
					Database.getLogger().log(Level.INFO, "Connection aborted with undefined client.");
				}
			} catch (IOException e) {}
			try {
				closeConnections();
			} catch (IOException | InterruptedException e1) {
				Database.getLogger().log(Level.WARNING, "Unable to close connection with client, attempting in 5 seconds...", e1);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e2) {
					Database.getLogger().log(Level.WARNING, "Unable to wait, skipping tiemout...", e2);
				}
				try {
					closeConnections();
				} catch (IOException | InterruptedException e2) {
					Database.getLogger().log(Level.WARNING, "Unable to close connection with client, attempting in 10 seconds...", e2);
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e3) {
						Database.getLogger().log(Level.WARNING, "Unable to wait, skipping tiemout...", e3);
					}
					try {
						closeConnections();
					} catch (IOException | InterruptedException e4) {
						Database.getLogger().log(Level.WARNING, "Unable to close connection with client, leaving socket open.", e4);
					}
				}
				
			}
        }
    }
    
    private void doIdentify() throws Exception{
		out.writeObject(ResponseCode.CONTINUE);
		Object obj = in.readObject();
		if(!(obj instanceof String)){
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
			return;
		}
		out.writeObject(MemberIdentifier.getMember((String) obj));
	}

	private void doUpdateEvent() throws Exception {
		out.writeObject(ResponseCode.CONTINUE);
		Object obj = in.readObject();
		if(!(obj instanceof Event)){
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
			return;
		}
		Event e = (Event) obj;
		Database.getEvent(e.getUUID()).setDetails(e);
		out.writeObject(ResponseCode.SUCCESS);
	}
	
	public void doSwitchArchive() throws Exception{
		out.writeObject(ResponseCode.CONTINUE);
		Object obj = in.readObject();
		if(!(obj instanceof Event)){
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
			return;
		}
		Event e = (Event) obj;
		System.out.println("ARCHIVE: "+e.isArchived());
		if(e.isArchived()){
			Database.removeEvent(e.getUUID());
			Database.addArchivedEvent(e);
		}else{
			Database.removeArchivedEvent(e.getUUID());
			Database.addEvent(e);
		}
		out.writeObject(ResponseCode.SUCCESS);
	}

	private void closeConnections() throws IOException, InterruptedException{
    	in.close();
		out.close();
		Thread.currentThread().join();
    }
    
    private void doResetPassword(boolean self) throws Exception{
    	Member m;
    	if(self){
    		m = MemberIdentifier.getMember(laPersona);
    	}else{
    		if(laPersona!=null && !MemberIdentifier.getMember(laPersona).isAdministrative()){
    			out.writeObject(ResponseCode.UNAUTHORIZED);
    			return;
    		}
    		out.writeObject(ResponseCode.MEMBER);
        	Object obj = in.readObject();
        	if(obj instanceof UUID){
        		if(MemberIdentifier.getMember((UUID) obj)==null){
        			out.writeObject(ResponseCode.INCORRECT_LOGIN);
        			return;
        		}
        		m = MemberIdentifier.getMember((UUID) obj);
        	}else{
        		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
        		return;
        	}
    	}
    	String encryptedPassword = Encrypter.generatePassword();
    	m.setPassword(Encrypter.decrypt(encryptedPassword));
    	m.save();
		out.writeObject(ResponseCode.SUCCESS);
    }
    
    private void doSetPassword(boolean self) throws Exception{
    	Member m = MemberIdentifier.getMember(laPersona);
    	if(!self){
    		if(!MemberIdentifier.getMember(laPersona).isAdministrative()){
    			out.writeObject(ResponseCode.UNAUTHORIZED);
    			return;
    		}
    		out.writeObject(ResponseCode.MEMBER);
        	Object obj = in.readObject();
        	if(obj instanceof UUID){
        		if(MemberIdentifier.getMember((UUID) obj)==null){
        			out.writeObject(ResponseCode.INCORRECT_LOGIN);
        			return;
        		}
        		m = MemberIdentifier.getMember((UUID) obj);
        	}else{
        		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
        		return;
        	}
    	}
    	out.writeObject(ResponseCode.CONTINUE);
    	Object obj = in.readObject();
    	if(!(obj instanceof String)){
    		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
    		return;
    	}
    	if(!m.getPassword().equals((String) obj)){
    		out.writeObject(ResponseCode.INCORRECT_LOGIN);
    		return;
    	}
    	out.writeObject(ResponseCode.CONTINUE);
    	Object obj2 = in.readObject();
    	if(!(obj2 instanceof String)){
    		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
    		return;
    	}
    	m.setPassword((String) obj2);
    	m.save();
		out.writeObject(ResponseCode.SUCCESS);
    }
    
    private void doGetEvent() throws Exception{
    	out.writeObject(ResponseCode.CONTINUE);
    	Object obj = in.readObject();
    	if(!(obj instanceof UUID)){
    		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
    	}
    	out.writeObject(Database.getEvent((UUID) obj));
    }
    private void doGetMember() throws Exception{
    	out.writeObject(ResponseCode.CONTINUE);
    	Object obj = in.readObject();
    	if(!(obj instanceof String)){
    		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
    		return;
    	}
    	out.writeObject(MemberIdentifier.getMemberByEmail((String) obj));
    }
    private void doSendEmails() throws Exception{
    	if(laPersona!=null && !MemberIdentifier.getMember(laPersona).isAdministrative()){
    		out.writeObject(ResponseCode.UNAUTHORIZED);
    	}
    	out.writeObject(ResponseCode.CONTINUE);
    	Object obj = in.readObject();
    	if(!(obj instanceof ArrayList<?>)){
    		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
    		return;
    	}
    	if(!Database.validatePassword()){
    		out.writeObject(ResponseCode.UNDEFINED_FAILURE);
    		return;
    	}
    	ArrayList<Email> emails = (ArrayList<Email>) obj;
    	new Thread(new EmailSystem(emails)).start();
    	out.writeObject(ResponseCode.SUCCESS);
    }
    
    private void doUpdateEmail(boolean self) throws Exception{
    	Member m = MemberIdentifier.getMember(laPersona);
    	if(!self){
    		if(!MemberIdentifier.getMember(laPersona).isAdministrative()){
    			out.writeObject(ResponseCode.UNAUTHORIZED);
    			return;
    		}
    		out.writeObject(ResponseCode.MEMBER);
        	Object obj = in.readObject();
        	if(obj instanceof UUID){
        		if(MemberIdentifier.getMember((UUID) obj)==null){
        			out.writeObject(ResponseCode.INCORRECT_LOGIN);
        			return;
        		}
        		m = MemberIdentifier.getMember((UUID) obj);
        	}else{
        		out.writeObject(ResponseCode.UNEXPECTED_PACKET);
        		return;
        	}
    	}
		out.writeObject(ResponseCode.CONTINUE);
		Object obj = in.readObject();
		if(obj instanceof String){
			m.setEmail((String) obj);
			m.save();
			out.writeObject(ResponseCode.SUCCESS);
		}else{
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
			return;
		}
    }
    
    private void doAddEvent() throws Exception{
    	if(!MemberIdentifier.getMember(laPersona).isAdministrative()){
			out.writeObject(ResponseCode.UNAUTHORIZED);
			return;
		}
    	Event e = new Event(Database.generateEventUUID(), new ArrayList<Member>(), new ArrayList<String>(), "undefName", "undefAddress", "undefDate", "undefTime", 0, -1, false);
    	Database.addEvent(e);
    	System.out.println(Database.getEvents());
    	e.save();
    	out.writeObject(e);
    }
    
    private void doRemoveEvent() throws Exception{
    	if(!MemberIdentifier.getMember(laPersona).isAdministrative()){
			out.writeObject(ResponseCode.UNAUTHORIZED);
			return;
		}
    	out.writeObject(ResponseCode.CONTINUE);
    	Object inc = in.readObject();
		if(inc instanceof Event){
			Event e = (Event) inc;
			if(!Database.removeEvent(e.getUUID())){
				if(!Database.removeArchivedEvent(e.getUUID())){
					out.writeObject(ResponseCode.UNDEFINED_FAILURE);
				}
			}
			e.delete();
			out.writeObject(ResponseCode.SUCCESS);
		}else{
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
		}
    }
    
    private void doAddHourLog() throws Exception{
    	out.writeObject(ResponseCode.CONTINUE);
    	Object inc = in.readObject();
		if(inc instanceof HourLog){
			HourLog log = (HourLog) inc;
			MemberIdentifier.getMember(laPersona).getHourSheet().add(log);
			out.writeObject(ResponseCode.SUCCESS);
		}else{
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
		}
    }
    
    private void doRemoveHourLog() throws Exception{
    	out.writeObject(ResponseCode.CONTINUE);
    	Object inc = in.readObject();
		if(inc instanceof HourLog){
			HourLog log = (HourLog) inc;
			MemberIdentifier.getMember(laPersona).getHourSheet().remove(log);
			out.writeObject(ResponseCode.SUCCESS);
		}else{
			out.writeObject(ResponseCode.UNEXPECTED_PACKET);
		}
    }
}