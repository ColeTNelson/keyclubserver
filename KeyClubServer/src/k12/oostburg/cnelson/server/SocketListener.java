package k12.oostburg.cnelson.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;

import k12.oostburg.cnelson.statics.Database;

public class SocketListener implements Runnable {
	public void run(){
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(9000);
		} catch (IOException e) {
			Database.getLogger().log(Level.SEVERE, "Unable to start SocketListener, attempting in 5 seconds...", e);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				Database.getLogger().log(Level.WARNING, "Unable to wait, skipping timeout...", e1);
			}
			try {
				socket = new ServerSocket(9000);
			} catch (IOException e1) {
				Database.getLogger().log(Level.SEVERE, "Unable to start SocketListener, attempting in 10 seconds...", e1);
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e2) {
					Database.getLogger().log(Level.WARNING, "Unable to wait, skipping timeout...", e2);
				}
				try {
					socket = new ServerSocket(9000);
				} catch (IOException e2) {
					Database.getLogger().log(Level.SEVERE, "Unable to start SocketListener, PERMANENT FAILURE.\nIs another instance running?\nTry restarting the system or contacting your system administrator", e2);
				}
			}
		}
		try {
			while (true) {
				new Thread(new Handler(socket.accept())).start(); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	     }
	}
}